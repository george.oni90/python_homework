# python homework
 ## Scope

TBA


## Usage
```bash
usage: script.py [-h] -f FILE [-d "Sun 07:21 AM"] [--log LOG_LEVEL]

Find my lunch location

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Provide File Location
  -d "Sun 07:21 AM", --datetime "Sun 07:21 AM"
                        Provide the Date-Time for the restaurant in the
                        format: Mon 02:20 PM
  --log LOG_LEVEL       Provide the logging level (default=WARNING)
                        [DEBUG,INFO,WARNING,ERROR,CRITICAL]
```

## Run examples


In the first examle we run in it in the basic format :
```bash
[root@hpmg8 python_homework]# ./script.py -f restaurants.csv  -d "Mon 00:01 AM"
Available locations for the specified Day and Time
Day: Mon at 00:01 AM

No location are available in the specified day & time
```

The second example contains a list of available places to eat, or to order.
```bash
[root@hpmg8 python_homework]# ./script.py -f restaurants.csv  -d "Mon 10:01 AM"
Available locations for the specified Day and Time
Day: Mon at 10:01 AM

Location: Herbivore
Opening Hours: Mon-Thu, Sun 9 am - 10 pm  / Fri-Sat 9 am - 11 pm

Location: Colombini Italian Cafe Bistro
Opening Hours: Mon-Fri 12 pm - 10 pm  / Sat-Sun 5 pm - 10 pm

Location: Tong Palace
Opening Hours: Mon-Fri 9 am - 9:30 pm  / Sat-Sun 9 am - 10 pm

Location: Santorini's Mediterranean Cuisine
Opening Hours: Mon-Sun 8 am - 10:30 pm

[root@hpmg8 python_homework]#
```

In the final example you have a more detailed run of the script (debug mode):
``` bash
[root@hpmg8 python_homework]# ./script.py -f restaurants.csv  -d "Mon 10:01 AM" --log DEBUG
INFO:Using file: restaurants.csv
DEBUG:Validate FilePath: restaurants.csv OK!
INFO:CSV Check on file: restaurants.csv PASSED!
INFO:Using time: Mon 10:01 AM
DEBUG:DATETIME Day Value - Mon - is a valid calendar
DEBUG:DATETIME Hour Value - 10 - is valid
DEBUG:DATETIME Minute Value - 01 - is valid
DEBUG:DATETIME Day Period - AM - is valid
Available locations for the specified Day and Time
Day: Mon at 10:01 AM

DEBUG:find_my_lunch - Hit day for location: "Kushi Tsuru"
DEBUG:find_my_lunch - Hit day for location: "Osakaya Restaurant"
DEBUG:find_my_lunch - Hit day for location: "The Stinking Rose"
DEBUG:find_my_lunch - Hit day for location: "McCormick & Kuleto's"
DEBUG:find_my_lunch - Hit day for location: "Mifune Restaurant"
DEBUG:find_my_lunch - Hit day for location: "The Cheesecake Factory"
DEBUG:find_my_lunch - Hit day for location: "New Delhi Indian Restaurant"
DEBUG:find_my_lunch - Hit day for location: "Iroha Restaurant"
DEBUG:find_my_lunch - Hit day for location: "Rose Pistola"
DEBUG:find_my_lunch - Hit day for location: "Alioto's Restaurant"
DEBUG:find_my_lunch - Hit day for location: "Canton Seafood & Dim Sum Restaurant"
DEBUG:find_my_lunch - Hit day for location: "All Season Restaurant"
DEBUG:find_my_lunch - Hit day for location: "Bombay Indian Restaurant"
DEBUG:find_my_lunch - Hit day for location: "Sam's Grill & Seafood Restaurant"
DEBUG:find_my_lunch - Hit day for location: "2G Japanese Brasserie"
DEBUG:find_my_lunch - Hit day for location: "Restaurant Lulu"
DEBUG:find_my_lunch - Hit day for location: "Sudachi"
DEBUG:find_my_lunch - Hit day for location: "Hanuri"
DEBUG:find_my_lunch - Hit day for location: "Herbivore"
Location: Herbivore
Opening Hours: Mon-Thu, Sun 9 am - 10 pm  / Fri-Sat 9 am - 11 pm


```

 

> Written with [StackEdit](https://stackedit.io/).
