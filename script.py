#!/bin/python

import sys, os, argparse
import logging, re
import csv, datetime, calendar

#Function used to validate the correct lation of the file
def validate_file_path(logger,filepath):
  if os.path.exists(filepath) == True :
     logger.debug("Validate FilePath: %s OK!",filepath)
  else:
     logger.error("Validate FilePath: %s NOK! File not found!", filepath)

#Function used to validate file as a csv file
def validate_csv(logger,filepath):
  csvfile = open(filepath, 'rb')
  try:
    dialect = csv.Sniffer().sniff(csvfile.read(1024))
  except csv.Error:
    logger.error("File %s is not a valid CSV file!",filepath)
  logger.info("CSV Check on file: %s PASSED!",filepath)

def logging_config(loglevel):
  #Create a new logger
  logger = logging.getLogger('example_logger')
  #Define the loffing format and convert the logging level to number
  formatter = logging.Formatter('%(levelname)s:%(message)s')
  log_numeric_level = getattr(logging, loglevel.upper(), None)
  #Check for correct logging level and 
  if not isinstance(log_numeric_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)
  logger.setLevel(loglevel)

  #Define the logging handler
  handler = logging.StreamHandler()
  handler.setLevel(loglevel)
  handler.setFormatter(formatter)
  logger.addHandler(handler)
  return logger

def check_datetime_imput_format(logger,value):
  import re
  hour_range = ["%.2d" % i for i in range(13)]
  minute_range = ["%.2d" % i for i in range(60)]
  #Check for correct DayOfWeek imput
  if value.split()[0].lower() in map(lambda x:x.lower(),list(calendar.day_abbr)): 
     logger.debug("DATETIME Day Value - %s - is a valid calendar",value.split()[0])
  else: logger.error("DATETIME Day Value - %s - not in calendar!\n Plase use one of the following values: %s",value.split()[0], list(calendar.day_abbr))
  #Check for correct Hour:Minute imput
  if value.split()[1].split(':')[0] in hour_range: 
     logger.debug("DATETIME Hour Value - %s - is valid", value.split()[1].split(':')[0])
  else: logger.error("DATETIME Hour Value - %s - is not in valid range!\n Please use the following values: %s", value.split()[1].split(':')[0], hour_range)
  if  value.split()[1].split(':')[1] in minute_range:
     logger.debug("DATETIME Minute Value - %s - is valid", value.split()[1].split(':')[1])
  else: logger.error("DATETIME Hour Value - %s - is not in valid range!\n Please a value between: [00 - 60]", value.split()[1].split(':')[1])
  #Check day zone AM or PM
  if value.split()[2].lower() in map(lambda x:x.lower(),['AM','PM']):
     logger.debug("DATETIME Day Period - %s - is valid", value.split()[2])
  else: logger.error("DATETIME Day Period - %s - is not valid !\n Please use one of the following values [AM,PM]",value.split()[2])

def find_my_lunch(csv_file,open_datetime,logger):
  #Define a dictionaty mapping between the day abbreviation and the day number
  days = dict(zip(map(lambda x:x.lower(),list(calendar.day_abbr)), range(len(list(calendar.day_abbr)))))
  
  #Define the matching paters to extract the Day information
  pattern1 = re.compile("[a-zA-Z]+-[a-zA-Z]+") #(Sun-Mon)
  pattern2 = re.compile("^( ?)[a-zA-Z][a-zA-Z][a-zA-Z]( )") #(Sum)

  print("Available locations for the specified Day and Time")
  print("Day: "+open_datetime.split()[0]+" at "+open_datetime.split()[1]+' '+open_datetime.split()[2]+'\n')
  with open(csv_file, mode='r') as csv_file:
     reader = csv.reader(csv_file, delimiter=',')
     tracer_check=0
     for row in reader:
       if row:
         tracer=0
         for item in row[1].split('/'):
             if open_datetime.split()[0].lower() in item.lower():
                logger.debug('find_my_lunch - Hit day for location: "'+row[0]+'"')
                if find_my_lunch_by_hour(item.lower(),open_datetime.split()[1]+' '+open_datetime.split()[2]): tracer += 1
             else:
                if pattern1.match(item.lower()): 
                  if ( int(days[open_datetime.split()[0].lower()]) > int(days[pattern1.search(item.lower().strip()).group().split('-')[0]]) and 
                       int(days[open_datetime.split()[0].lower()]) < int(days[pattern1.search(item.lower().strip()).group().split('-')[1]]) ):
                     logger.debug('find_my_lunch - Hit day for location: "'+row[0]+'"')
                     if find_my_lunch_by_hour(item.lower(),open_datetime.split()[1]+' '+open_datetime.split()[2]): tracer += 1
         if tracer > 0:
            print("Location: "+row[0]+"\nOpening Hours: "+row[1]+'\n') 
            tracer_check += 1
            logger.info("Locatin Found! - %s", row[0])            
     if tracer_check == 0: print("No location are available in the specified day & time")           

def find_my_lunch_by_hour(data,time):
  #Define regex retection patterns for time interval
  pattern_full = re.compile("[0-9]+(:?)([0-9]*?) (am|pm) (-?) [0-9]+(:?)([0-9]*?) (am|pm)")
  pattern_hour = re.compile("[0-9]+(:)([0-9]*)")
  #Define the HOUR:MINUTE and TIME PERIOD (AM/PM)
  format = '%I:%M %p'
  #Define datetime variable format
  now = datetime.datetime.now()
  
  #Extract start and end time informatin from captured string
  start_time = pattern_full.search(data).group().split('-')[0].strip()
  end_time = pattern_full.search(data).group().split('-')[1].strip()

  #Check if hour:minute format is correct. If not correct it !
  if not pattern_hour.match(start_time.upper()): start_time = start_time.split()[0]+':00 '+start_time.split()[1]
  if not pattern_hour.match(end_time.upper()): end_time = end_time.split()[0]+':00 '+end_time.split()[1]
  #Compensate for the 12 Hour difference between AM and PM
  if ( 'pm' in start_time.lower() and int(start_time.split()[0].split(':')[0]) != 12 ): 
      start_time = str(int(start_time.split()[0].split(':')[0])+12)+':'+start_time.split()[0].split(':')[1]+' '+start_time.split()[1]
  elif ( 'pm' in start_time.lower() and int(start_time.split()[0].split(':')[0]) == 12 ):
      start_time = '00:'+start_time.split()[0].split(':')[1]+' '+start_time.split()[1]
  if ('pm' in end_time.lower() and int(end_time.split()[0].split(':')[0]) != 12): 
      end_time = str(int(end_time.split()[0].split(':')[0])+12)+':'+end_time.split()[0].split(':')[1]+' '+end_time.split()[1]
  elif ( 'pm' in end_time.lower() and int(end_time.split()[0].split(':')[0]) == 12 ):
      end_time = '00:'+end_time.split()[0].split(':')[1]+' '+end_time.split()[1]
  if ( 'pm' in time.lower() and int(time.split()[0].split(':')[0]) != 12):
      time = str(int(time.split()[0].split(':')[0])+12)+':'+time.split()[0].split(':')[1]+' '+time.split()[1] 
  elif ( 'pm' in time.lower() and int(time.split()[0].split(':')[0]) == 12 ):
       time = '00:'+time.split()[0].split(':')[1]+' '+time.split()[1]



  #Convert date related strings to datetime objects. This will ensure the comparison will be resolved !
  start = now.replace(hour = int(start_time.split()[0].split(':')[0]), minute = int(start_time.split()[0].split(':')[0]))
  end = now.replace(hour = int(end_time.split()[0].split(':')[0]), minute = int(end_time.split()[0].split(':')[0]))
  time = now.replace(hour = int(time.split()[0].split(':')[0]), minute = int(time.split()[0].split(':')[0]))
  if ( time > start and time < end ):  return 1
  return 0  


def main(): 
  #Define Parser
  parser = argparse.ArgumentParser(description='Find my lunch location')
  parser.add_argument("-f","--file",dest="csv_file",help="Provide File Location", metavar="FILE",required=True)
  parser.add_argument("-d","--datetime",dest="open_datetime",help="Provide the Date-Time for the restaurant in the format: Mon 02:20 PM", 
                      metavar='"'+calendar.day_abbr[datetime.datetime.now().weekday()]+" "+datetime.datetime.now().strftime("%I:%M %p")+'"',
                      default=calendar.day_abbr[datetime.datetime.now().weekday()]+" "+datetime.datetime.now().strftime("%I:%M %p"))
  parser.add_argument("--log",dest="loglevel",help="Provide the logging level (default=WARNING) [DEBUG,INFO,WARNING,ERROR,CRITICAL] ", 
                     metavar="LOG_LEVEL",default='WARNING', choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"])
  args = parser.parse_args()

  #Configure logger
  logger = logging_config(args.loglevel)
  
  #Start checks
  logger.info("Using file: %s",args.csv_file)
  validate_file_path(logger,args.csv_file)
  validate_csv(logger,args.csv_file)

  #Execute main function
  logger.info("Using time: %s",args.open_datetime)
  check_datetime_imput_format(logger,args.open_datetime)
  find_my_lunch(args.csv_file,args.open_datetime,logger)

if __name__ == "__main__":
  main()
